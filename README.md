# CreditDebit

To use this application the file `public/config/environment.js` must be created with required environment information. It is posible to use `public/config/environment.template.js` as example.    
This mechanism allows to change environment variables even after compile application    

### Deployment
next command allow run **React** server in developer mode

```
npm start
```

### build application

```
npm run build
```

### Create an optimized production build

```
npm run build --production
```