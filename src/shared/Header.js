
import React from 'react';

import { 
    AppBar, 
    Toolbar,
    Typography,
    Button
} from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    root: {
        '& .MuiTextField-root': {
            margin: theme.spacing(2),
            width: '25ch',
        }
    },
    title: {
      flexGrow: 1,
    },
}));


export default (props)=>{
    const classes = useStyles();
    return <AppBar position="static">
        <Toolbar>
            <Typography 
                variant="h6" 
                className={classes.title}>
                CreditDebit
            </Typography>
            <Button color="inherit" href="/">All</Button>
            <Button color="inherit" disabled href="/create">Create</Button>
        </Toolbar>
    </AppBar>
}

