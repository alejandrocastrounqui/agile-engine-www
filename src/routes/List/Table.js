import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Typography,
  ExpansionPanel,
  ExpansionPanelSummary,
  ExpansionPanelDetails
} from '@material-ui/core';

import ExpandMoreIcon from '@material-ui/icons/ExpandMore';


const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '33.33%',
    flexShrink: 0,
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    flexGrow: '0.99',
    textAlign: 'right'
  },
}));

export default function(props) {
  const classes = useStyles();
  return (
  <div className={classes.root}>
    <ExpansionPanel>
      <ExpansionPanelSummary>
        <Typography className={classes.heading}>Type</Typography>
        <Typography className={classes.secondaryHeading}>Amount</Typography>
      </ExpansionPanelSummary>
    </ExpansionPanel>
    
    {props.transactions.map((transaction) => (
      <ExpansionPanel key={transaction.id}>
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1bh-content"
          id="panel1bh-header"
        >
          <Typography className={classes.heading}>{transaction.type}</Typography>
          <Typography className={classes.secondaryHeading}>
            
            {(transaction.type === 'credit'? '' : '-') + '$ ' + transaction.amount.toFixed(2)}
          </Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <div style={{
            flexDirection: 'column'
          }}>
            <Typography>
              <span>ID: </span>
              <span>{transaction.id}</span>
            </Typography>
            <Typography>
              <span>Effective date: </span>
              <span>{transaction.effectiveDate}</span>
            </Typography>
          </div>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    ))}

  </div>
  )
}
