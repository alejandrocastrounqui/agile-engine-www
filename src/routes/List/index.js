import React, { useState, useEffect } from 'react';
import Table from './Table';

const apiUrl = window['API_URL']

export default function Detail() {
    const [transactions, setTransactions] = useState([]);
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(null);

    useEffect(() => {
        fetch(`${apiUrl}/transactions`)
        .then(res => res.json())
        .then(transactions => {
            setTransactions(transactions)
            setLoading(false)
        })
        .catch(reason=>{
            setError(reason)
        })
    }, [])
    var content;
    if (loading){
        content = <span>loading</span>
    }
    else if(error){
        content = <span>JSON.stringify(error, null, error)</span>
    }
    else if(transactions && transactions.length){
        content = <Table transactions={transactions}/>
    }
    else{
        content = <span>No hay transacciones disponibles</span>
    }
    return <div>
        <h2>Transactions</h2>
        {content}

    </div>
     
    
    
}


