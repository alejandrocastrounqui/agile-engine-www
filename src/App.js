import React from 'react';
import './App.css';
import {
  BrowserRouter,
  Switch,
  Route
} from "react-router-dom";

import { MuiThemeProvider, CssBaseline, Card } from "@material-ui/core";
import theme from "./theme";
import GlobalStyles from "./GlobalStyles";
import List from './routes/List/index'
import Header from './shared/Header'

function App() {
  return (
    <BrowserRouter>
      <MuiThemeProvider theme={theme}>
        <CssBaseline />
        <GlobalStyles />   
        <Header />    
        <Card style={{
            margin:"10px",
            padding:"0 20px 20px 20px"
            }}>
          <Switch>
            <Route exact path="/">
              <List />
            </Route>
          </Switch>
        </Card>
      </MuiThemeProvider>
    </BrowserRouter>
  );
}

export default App;
